/***********************************************************************************
* FILE: Timer.c
*
* Author: Olivier Belisle (BELO21098902)
* Author: Felix Champagne-Lapointe (CHAF11058908)
***********************************************************************************/
#include "Timer.h"

/***********************************************************************************
* VARIABLES
***********************************************************************************/


/***********************************************************************************
* INTERRUPT FUNCTIONS
***********************************************************************************/
/* signal handler for timer0 interrupt */
ISR(TIMER0_OVF_vect)
{
	/*
		//Si le data est pret
		//
		if(ADC_MeanFlag)
		{
			
			CLEARBYTE(ADC_MeanFlag);
		}
		else
		{
			
		}
	      1 � Calculer la moyenne des �chantillons recueillit par l�ADC.
	      2 � Ex�cuter la fonction CalculPWM.
	      3 � Mettre � jour les PWM et les bits de direction des moteurs.
	*/
	
}

/* signal handler for timer1 interrupt */
ISR(TIMER1_OVF_vect)
{
	//IRQ Call au 5ms
	CalculPWM(Robot_Info_FP.vitesse,Robot_Info_FP.angle,Robot_Info_FP.ADC_VG, Robot_Info_FP.ADC_VD, &Robot_Info_FP.Duty_G, &Robot_Info_FP.Duty_D);
	//Se fier sur le PWM retourner pour savoir la direction ?
	//Cela veut dire qune roue peut avancer et lautre reculer
	
	Convert_Robot_Info_Calculated_PWM(&Robot_Info, &Robot_Info_FP);
	
	SetDirection(Robot_Info.direction);
	
	SetPWM(Robot_Info.Duty_D, Robot_Info.Duty_G);
	
	Robot_Info.dutyCycle_Done_Flag = TRUE;
}

/* signal handler for timer2 interrupt */
ISR(TIMER2_OVF_vect)
{

	
}

/***********************************************************************************
* FUNCTIONS
***********************************************************************************/
/* initialize Timers */
void Init_Timer(void)
{

}

void InitPWM(void)
{
	//Set to Fast PWM
	//Set OC1A/OC1B on compare match, clear OC1A/OC1B at TOP
	//Set prescaler to divide by 64 the CPU freq of 16 MHZ
	TCCR1A = (1 << COM1A1) | (1 << COM1A0) | (1 << COM1B1) | (1 << COM1B0) | (1 << WGM11) | (0 << WGM10);
	TCCR1B = (1 << WGM13) | (1 << WGM12) | (1 << CS11) | (1 << CS10);

	//Must be set after setting WGM bits in TCCR1A and TCCR1B
	ICR1 = PWM_TOP;

	//Enable Overflow interrupt
	TIMSK = (1 << TOIE1);

	//Initially set PWM to off
	SetPWM(0, 0);
}

void SetPWM(uint8_t DutyCycle_D, uint8_t DutyCycle_G)
{
	
	volatile uint16_t temp_Compare = ((DutyCycle_D * PWM_TOP) / 100); 
	OCR1A = temp_Compare;
	
	temp_Compare = ((DutyCycle_G * PWM_TOP) / 100); 
	OCR1B = temp_Compare;

}

/*void SetPWM(uint16_t Period, uint8_t Direction)
{

	//0x4E1 is 100%

	//receive in %

	//Commande * 0x4E1
	
}*/
