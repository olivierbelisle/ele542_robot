/***********************************************************************************
* FILE: UART.h
*
* Author: Olivier Belisle (BELO21098902)
* Author: Felix Champagne-Lapointe (CHAF11058908)
***********************************************************************************/

#ifndef __UART_H__
#define __UART_H__

/***********************************************************************************
* INCLUDES
***********************************************************************************/
#include "define.h"
#include <avr/io.h>
#include <avr/interrupt.h>

/***********************************************************************************
* VARIABLES
***********************************************************************************/
extern robot_info Robot_Info;

/***********************************************************************************
* DEFINES
***********************************************************************************/
#define UART_BAUD_RATE 9600         /* 9600 baud */
#define UART_BAUD_SELECT (F_CPU/(UART_BAUD_RATE*16l)-1)

#define COMMANDE_NORMALE 0xF1
#define ARRET_OBLIGATOIRE 0xF0

#define DEBUG_MSG_START 0xFE
#define DEBUG_MSG_STOP 0xFF

#define MESSAGE_SENT UCSRA & TXC

/* Defines possible RX type    */
typedef enum RX_Status            
{
	idle = 0,       /* Waiting for a new command       */
	get_Vitesse,    /* Waiting to receive the speed      */
	get_Angle		/* Waiting to receive the angle      */
} rx_States;

/***********************************************************************************
* FUNCTIONS PROTOTYPES
***********************************************************************************/
/* send buffer <buf> to uart */
void Send_UART(uint8_t *buf, uint8_t size);

/* initialize UART */
void Init_UART(void);

//Debug_messages with value
void Debug_Send_Val(uint8_t * Message, uint16_t Valeur);

//Debug_messages with Float value
void Debug_Send_FP(uint8_t * Message, float Valeur);

//Debug_messages without value
void Debug_Send_Msg(uint8_t * Message);

#endif //__UART_H__
