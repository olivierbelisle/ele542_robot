/************************************************************************************
* File : TWI.c
* By   : F�lix Champagne-Lapointe
* By   : Olivier Belisle
************************************************************************************/

#include "define.h"
#include "TWI.h"

uint8_t* data_out;
uint8_t* data_in;
uint8_t data_lenght = 0;
uint8_t counter = 0;

void InitTWI(void)
{
	TWBR = 0x0C;
	CLEARBIT(TWSR,TWPS0);
	CLEARBIT(TWSR,TWPS1);
	SETBIT(TWCR, TWIE);
	SETBIT(TWCR, TWEN);
}

void Start_Write_Communication(uint8_t addr, uint8_t* data, uint8_t lenght)
{
	if(data_out != 0x00)
		free(data_out);
		
	data_out = malloc(lenght * sizeof(uint8_t));
	memcpy((uint8_t*)data_out, (uint8_t*)data, lenght);
	TWDR = addr | WRITE;
	data_lenght = lenght;

	SETBIT(TWCR, TWEA);
	SETBIT(TWCR, TWSTA);
}

void Start_Read_Communication(uint8_t addr, uint8_t* data, uint8_t lenght)
{
	if(data_in != 0x00)
	free(data_in);
	
	data_in = malloc(lenght * sizeof(uint8_t));
	TWDR = addr | READ;
	data_lenght = lenght;
	
	SETBIT(TWCR, TWEA);
	SETBIT(TWCR, TWSTA);
}

//TO DO
//Gestion d'erreur avec timer (~50ms) au besoin

 // TWI interrupt routine
ISR(TWI_vect)
{
	switch(TWSR & TWSR_MASK)
	{
		case START_STATUS:
		
			CLEARBIT(TWCR,TWSTA);
		
		break;
		
		case SLA_W_ACK_STATUS:
		
			TWDR = data_out[counter ++];
		
		break;
		
		case ACK_W_RECEIVE_STATUS:
		
			if(counter < data_lenght)
			{
				TWDR = data_out[counter ++];
			}
			else
			{
				TWDR = data_out[counter];
				counter = 0;
				SETBIT(TWCR, TWSTO);
			}
		
		break;
		
		case SLA_R_ACK_STATUS:	
		
			CLEARBIT(TWCR,TWSTA);
		
		break;
		
		case SLA_R_NACK_STATUS:					SETBIT(TWCR, TWSTO);				break;				case ACK_R_RECEIVE_STATUS:				if(counter < data_lenght)			data_in[counter ++] = TWDR;		else		{			data_in[counter] = TWDR;			CLEARBIT(TWCR, TWEA);			SETBIT(TWCR, TWSTO);		}				break;				case NACK_R_RECEIVE_STATUS:
		
			SETBIT(TWCR, TWSTO);
		
		break;	
	}
	
	SETBIT(TWCR, TWINT);
}