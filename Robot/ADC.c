/***********************************************************************************
* FILE: Timer.c
*
* Author: Olivier Belisle (BELO21098902)
* Author: Felix Champagne-Lapointe (CHAF11058908)
***********************************************************************************/

#include "ADC.h"

volatile uint8_t cpt = 0;

/***********************************************************************************
* FUNCTIONS
***********************************************************************************/

void InitADC(void) //PDF p.214
{

	//ADCSRA = 0x00;
	
	//Disable Analog comparator
	ACSR = ( 1 << ACD);

	ADMUX = 0x00;// AREF, Internal Vref turned off

	//ADPS2..ASDP0 = 111  -> ADC Prescaler Division Factor = 128, Converting as long as ADSC set to 1
	//ADC Auto Trigger Enable, convert on positive edge
	ADCSRA = ( 1 << ADEN) | ( 1 << ADIE) | ( 1 << ADSC) | ( 1 << ADPS0) | ( 1 << ADPS1) | ( 1 << ADPS2) | ( 1 << ADATE);
	
	//Free Running mode = 000
	SFIOR = ( 0 << ADTS0) | ( 0 << ADTS1) | ( 0 << ADTS2);

	sConvert.ready = FALSE;

	//SetADCChannel(uint8_t channel)
}

void SetADCChannel(uint8_t channel)
{
	ADMUX = channel & ADMUX_MASK;
	//SETBIT(ADMUX, channel & ADMUX_MASK);
}

void GetStructureValue(float* LeftValue, float* RightValue)
{
	if(sConvert.ready)
	{
		*LeftValue = sConvert.LeftMotorMean / MAX_ACQUISITION;
		*RightValue = sConvert.RightMotorMean / MAX_ACQUISITION;
		
		*LeftValue = (*LeftValue * 512.00f) - 1;
		*RightValue = (*RightValue * 512.00f) - 1;
		
		sConvert.LeftMotorMean = 0;
		sConvert.RightMotorMean = 0;
		sConvert.ready = FALSE;
		//To do remettre le compteur ADC a zero
	}
	else
	{
		*LeftValue = 0.00f;
		*RightValue = 0.00f;
		//sConvert.ready = FALSE;
	}
}

/***********************************************************************************
* INTERRUPT FUNCTIONS
***********************************************************************************/
ISR(ADC_vect)
{
	static uint8_t counter_test = 0;


	if((!sConvert.ready) && (cpt < MAX_ACQUISITION))
	{

		switch(ADMUX & ADMUX_MASK)
		{
			case MOTEUR_G:
			
				sConvert.LeftMotorMean += ADCW;
				SetADCChannel(MOTEUR_D);
				break;
			
			case MOTEUR_D:
			
				sConvert.RightMotorMean += ADCW;
				SetADCChannel(MOTEUR_G);
				cpt ++;
			
			break;
		}
	}
	else if(cpt >= MAX_ACQUISITION)
	{
		sConvert.ready = TRUE;
		cpt = 0;	
	}
}
