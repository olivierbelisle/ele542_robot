/***********************************************************************************
* FILE: Watchdog.h
*
* Author: Olivier Belisle (BELO21098902)
* Author: Felix Champagne-Lapointe (CHAF11058908)
***********************************************************************************/
 
#include "Define.h"

#ifndef WATCHDOG_H_
#define WATCHDOG_H_

#define WDP_15MS 0
#define WDP_30MS 1
#define WDP_60MS 2
#define WDP_120MS 3
#define WDP_250MS 4
#define WDP_500MS 5
#define WDP_1S 6
#define WDP_2S 7

#define SET_PRESCALER(VALUE) SETBYTE(WDTCR,VALUE | WDTCR)

void Enabled_Watchdog(void);
void Disabled_Watchdog(void);

#endif /* WATCHDOG_H_ */