/***********************************************************************************
* FILE: UART.c
*
* Author: Olivier Belisle (BELO21098902)
* Author: Felix Champagne-Lapointe (CHAF11058908)
***********************************************************************************/
#include "UART.h"

/***********************************************************************************
* VARIABLES
***********************************************************************************/
/* uart globals */
static volatile uint8_t *uart_data_ptr;
static volatile uint8_t uart_counter;
static volatile uint8_t debug_enabled = FALSE;

/***********************************************************************************
* INTERRUPT FUNCTIONS
***********************************************************************************/
/* signal handler for uart txd ready interrupt */
ISR(USART_TXC_vect)
{
	if(debug_enabled)
	{
		uart_data_ptr++;

		/* write byte to data buffer, always set debugging to off when a string finish to be sent */
		(--uart_counter) ? (UDR = *uart_data_ptr) : (debug_enabled = FALSE);
	}

	/*if (--uart_counter)
		UDR = *uart_data_ptr;*/
		
}



/* signal handler for receive complete interrupt */
ISR(USART_RXC_vect)
{
	static volatile rx_States RX_Steps = idle;
	volatile uint8_t RX_Data = UDR;

	//Receive UART messages
	switch (RX_Data)
	{
		
		case ARRET_OBLIGATOIRE:
			Robot_Info.direction = FREIN;
		case COMMANDE_NORMALE:	
			RX_Steps = get_Vitesse;
			break;
			
		default:
			switch (RX_Steps)
			{
				case get_Vitesse:
					Robot_Info.vitesse = RX_Data;					
					RX_Steps = get_Angle;
				break;
				
				case get_Angle:
					Robot_Info.angle = RX_Data;
					Robot_Info.teleguidage_Rx_Flag = TRUE;
					RX_Steps = idle;
				break;
				
				default:
					//Do nothing, error or reset to idle case
					RX_Steps = idle;
				break;
			}
		break;
	}

	//send echo of UDR after each RX
	if((!debug_enabled) && (!uart_counter))
		UDR = RX_Data;

}

/***********************************************************************************
* FUNCTIONS
***********************************************************************************/
/* send buffer <buf> to uart */
void Send_UART(uint8_t *buf, uint8_t size)
{
	if ((!debug_enabled) && (!uart_counter)) {
		uart_data_ptr = buf;          /* write first byte to data buffer */
		uart_counter = size;
		UDR = *buf;
		debug_enabled = TRUE;
	}
}

/* initialize UART */
void Init_UART(void)
{
   /* configure asynchronous operation, no parity, 1 stop bit, 8 data bits, Tx on rising edge */
   UCSRC = ((1<<URSEL)|(0<<UMSEL)|(0<<UPM1)|(0<<UPM0)|(0<<USBS)|(1<<UCSZ1)|(1<<UCSZ0)|(0<<UCPOL));
   /* enable RxD/TxD and ints */
   UCSRB = ((1<<RXCIE)|(1<<TXCIE)|(1<<RXEN)|(1<<TXEN)|(0<<UCSZ2));

//UX2 ? FCPU ?

	/* set baud rate */
	UBRRH = (uint8_t)(UART_BAUD_SELECT >> 8);
	UBRRL = (uint8_t)(UART_BAUD_SELECT & 0x00FF);
}

//Debug_messages with value
void Debug_Send_Val(uint8_t * Message, uint16_t Valeur)
{
	uint8_t Message_buffer[50]; //15 characters always present, leaves 33 characters for the message and data
	uint8_t Message_Length = sprintf(Message_buffer,"%c %s %d\n\r%c",DEBUG_MSG_START,Message,Valeur,DEBUG_MSG_STOP);
	Send_UART(Message_buffer, Message_Length);
}

//Debug_messages with float value
void Debug_Send_FP(uint8_t * Message, float Valeur)
{
	uint8_t Message_buffer[50]; //15 characters always present, leaves 33 characters for the message and data
	uint8_t Message_Length = sprintf(Message_buffer,"%cDebug: %s Val: %f\n\r%c",DEBUG_MSG_START,Message,Valeur,DEBUG_MSG_STOP);
	Send_UART(Message_buffer, Message_Length);
}

//Debug_messages without value
void Debug_Send_Msg(uint8_t * Message)
{
	uint8_t Message_buffer[50]; //15 characters always present, leaves 33 characters for the message
	uint8_t Message_Length = sprintf(Message_buffer,"%c%s\n\r%c",DEBUG_MSG_START,Message,DEBUG_MSG_STOP);
	Send_UART(Message_buffer, Message_Length);
}
