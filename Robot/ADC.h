/***********************************************************************************
* FILE: ADC.c
*
* Author: Olivier Belisle (BELO21098902)
* Author: Felix Champagne-Lapointe (CHAF11058908)
***********************************************************************************/

#ifndef ADC_H_
#define ADC_H_

/***********************************************************************************
* INCLUDES
***********************************************************************************/
#include "define.h"

/***********************************************************************************
* DEFINES
***********************************************************************************/
#define ADMUX_MASK 0x1F
#define MOTEUR_G 0x00
#define MOTEUR_D 0x01
#define MAX_ACQUISITION 12
#define TENBITS_MASK 0x3FF

/***********************************************************************************
* VARIABLES AND STRUCT
***********************************************************************************/
typedef struct
{
	uint8_t ready;
	uint16_t LeftMotorMean;
	uint16_t RightMotorMean;
} ADC_Convert;

volatile ADC_Convert sConvert;

/***********************************************************************************
* PROTOTYPES
***********************************************************************************/
void InitADC(void);
void SetADCChannel(uint8_t channel);
void GetStructureValue(float* LeftValue, float* RightValue);

#endif /* ADC_H_ */
