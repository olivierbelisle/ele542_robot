/***********************************************************************************
* FILE: Task_Gen.h
*
* Author: Olivier Belisle (BELO21098902)
* Author: Felix Champagne-Lapointe (CHAF11058908)
***********************************************************************************/

#ifndef TASK_GEN_H_
#define TASK_GEN_H_

/***********************************************************************************
* INCLUDES
***********************************************************************************/
#include "define.h"
#include "UART.h"
#include "Timer.h"
#include "TWI.h"

/***********************************************************************************
* FUNCTIONS PROTOTYPES
***********************************************************************************/

/* Regroup all the initializations needed here*/
void Init_Global(void);

void Convert_Robot_Info_To_Calculate_PWM(robot_info * Robot_Info_Ptr, robot_info_FP * Robot_Info_FP_Ptr);

void Convert_Robot_Info_Calculated_PWM(robot_info * Robot_Info_Ptr, robot_info_FP * Robot_Info_FP_Ptr);

#endif /* TASK_GEN_H_ */