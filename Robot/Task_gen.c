/***********************************************************************************
* FILE: Task_Gen.c
*
* Author: Olivier Belisle (BELO21098902)
* Author: Felix Champagne-Lapointe (CHAF11058908)
***********************************************************************************/

/***********************************************************************************
* INCLUDES
***********************************************************************************/
#include "define.h"
#include "UART.h"
#include "Timer.h"
#include "TWI.h"
#include "ADC.h"

/***********************************************************************************
* VARIABLES
***********************************************************************************/

/***********************************************************************************
* FUNCTION PROTOTYPES
***********************************************************************************/
void Init_Global(void)
{

	//Init Motor
	SETBYTE(DDRD,0xFE); //Set MOTOR (PORTD) to output except Rx (bit0), also set PWM to output

	//Init LED
	SETBYTE(LED_DDR,OUTPUT); //Set LED (PORTB) to output
	LED.Actif = LED_ON;
	LED.En_Attente = LED_ON;

	//InitTWI();

	//Init_Timer(); //Do Nothing for now
	
	PORTA = 0x00;
	DDRA = 0xFC;
	InitADC();

		
	Init_UART();

	//InitPWM();
	//Enable port A 0 and 1
	
	
	//To do
	//Call init_Timer();

	//Call init_PORT();
	
	//Call init_Watchdog();
}


void Convert_Robot_Info_To_Calculate_PWM(robot_info * Robot_Info_Ptr, robot_info_FP * Robot_Info_FP_Ptr)
{
	
	Robot_Info_FP_Ptr->vitesse = (Robot_Info_Ptr->vitesse / 100.00f) - 1.00f;
	Robot_Info_FP_Ptr->angle = INT_ANGLE_TO_FLOAT(Robot_Info_Ptr->angle);
	
	Robot_Info_Ptr->teleguidage_Rx_Flag = FALSE;
}


void Convert_Robot_Info_Calculated_PWM(robot_info * Robot_Info_Ptr, robot_info_FP * Robot_Info_FP_Ptr)
{
	Robot_Info_Ptr->Duty_D = PWM_TO_DUTY_CYCLE(Robot_Info_FP_Ptr->Duty_D);
	Robot_Info_Ptr->Duty_G = PWM_TO_DUTY_CYCLE(Robot_Info_FP_Ptr->Duty_G);

	Robot_Info_Ptr->dutyCycle_Done_Flag = FALSE;
}


void SetDirection(directions Direction)
{
	MOTOR.DIR_D1 = Direction & 0x01;
	MOTOR.DIR_D2 = Direction & 0x02;
	
	MOTOR.DIR_G1 = Direction & 0x01;
	MOTOR.DIR_G1 = Direction & 0x02;
}


