/************************************************************************************
* File : Robot.c
* By   : F�lix Champagne-Lapointe
* By   : Olivier Belisle
************************************************************************************/

/***********************************************************************************
* INCLUDES
***********************************************************************************/
#include "define.h"
#include "Task_Gen.h"

/***********************************************************************************
* GLOBAL VARIABLES
***********************************************************************************/
robot_info Robot_Info; //The main robot status variables
robot_info_FP Robot_Info_FP; //The main robot status variables for CalculPWM (converted in float)

/***********************************************************************************
* MAIN
***********************************************************************************/
int main(void)
{
	
	Init_Global();
	sei(); 
	

		//(counter_test > 254) ? (counter_test = 0) : (counter_test++);
		//PORTB = counter_test;
	
	
	while(1)
	{    
		static uint8_t vitessetest = 0;
		static uint8_t angletest = 0; //0 a 180, mettre sur 9 bit si on veut sur 360 deg                   
		//Enable init_UART() in Init_Global() and Try the two method for next lab

		//GetStructureValue(&Robot_Info_FP.ADC_VG, &Robot_Info_FP.ADC_VD);
		if(Robot_Info.vitesse != vitessetest)
		{

			Debug_Send_Val("MG", ADCW);
			//Debug_Send_Val("MD", GetADC());//Robot_Info_FP.ADC_VD);

			vitessetest = Robot_Info.vitesse;
		}
		
		/*if(Robot_Info.dutyCycle_Done_Flag)
			Convert_Robot_Info_Calculated_PWM(&Robot_Info, &Robot_Info_FP);*/

		


		/*if(Robot_Info.vitesse != vitessetest)
		{
			Debug_Send_Val("Vitesse", Robot_Info.vitesse);

			vitessetest = Robot_Info.vitesse;
		}

		if(Robot_Info.angle != angletest)
		{
			Debug_Send_Val("Angle", Robot_Info.angle);

			angletest = Robot_Info.angle;
		}*/


		/*if(Robot_Info.vitesse != vitessetest)
		{
			SetPWM(Robot_Info.vitesse / 2);

			vitessetest = Robot_Info.vitesse;
		}*/
		//Debug_Send_Msg("Robot Vitesse");
	}
}
