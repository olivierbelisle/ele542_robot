/***********************************************************************************
* FILE: Watchdog.c
*
* Author: Olivier Belisle (BELO21098902)
* Author: Felix Champagne-Lapointe (CHAF11058908)
***********************************************************************************/

#include "Watchdog.h"

void Enabled_Watchdog(void)
{
	SETBIT(WDTCR, WDE);
}

void Disabled_Watchdog(void)
{
	SETBIT(WDTCR, WDTOE);
	SETBIT(WDTCR, WDE);
	SETBYTE(WDTCR, 0x00);
}

ISR(WDT_vect)
{
}
