/************************************************************************************
* File : Define.h
* Author: Olivier Belisle (BELO21098902)
* Author: Felix Champagne-Lapointe (CHAF11058908)
************************************************************************************/
/***********************************************************************************
* INCLUDES
***********************************************************************************/
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <avr/iom32.h>
#include "stdlib.h"

#ifndef DEFINE_H_
#define DEFINE_H_

typedef enum direction {
	NEUTRE = 0,
	AVANT,
	ARRIERE,
	FREIN
}directions;

/***********************************************************************************
* STRUCTURES
***********************************************************************************/
typedef struct
{
	volatile directions direction;
	volatile uint8_t vitesse:8;
	volatile uint8_t angle:8;
	volatile uint16_t ADC_VG:10;
	volatile uint16_t ADC_VD:10;
	volatile uint8_t Duty_G;
	volatile uint8_t Duty_D;
	volatile uint8_t teleguidage_Rx_Flag;
	volatile uint8_t dutyCycle_Done_Flag;
}robot_info;

typedef struct
{
	volatile float direction;
	volatile float vitesse;
	volatile float angle;
	volatile float ADC_VG;
	volatile float ADC_VD;
	volatile float Duty_G;
	volatile float Duty_D;
}robot_info_FP;

//Faire des methode pour utiliser la vitesse et langle en faisant abstration des infos suivantes:
//vitesse qui varie de -100 a 100, en realite, nous avons des valeurs de 0 a 200
//angle qui varie de 0 a 360, en realite, nous avons des valeurs de 0 a 180

/*General PORT*/
typedef struct
{
	uint8_t b0:1;
	uint8_t b1:1;
	uint8_t b2:1;
	uint8_t b3:1;
	uint8_t b4:1;
	uint8_t b5:1;
	uint8_t b6:1;
	uint8_t b7:1; 
} port_bits;

/*LED signals*/
typedef struct
{
	uint8_t Not_Used:1;
	uint8_t Commande_Recue:1;
	uint8_t Obstacle_Sonar_D:1;
	uint8_t PING_Sonar_D:1;
	uint8_t Obstacle_Sonar_G:1;
	uint8_t PING_Sonar_G:1;
	uint8_t En_Attente:1;
	uint8_t Actif:1;
} led_bits;

/*Motor logic signals*/
typedef struct
{
	uint8_t Not_Used:2;
	uint8_t DIR_G1:1;
	uint8_t DIR_G2:1;
	uint8_t PWM_G:1;
	uint8_t PWM_D:1;
	uint8_t DIR_D1:1;
	uint8_t DIR_D2:1;
} motor_bits;



/***********************************************************************************
* DEFINES AND MACROS
***********************************************************************************/
#define DEBUG_MSG FALSE

#define BIT0 0x01
#define BIT1 0x02
#define BIT2 0x04
#define BIT3 0x08
#define BIT4 0x10
#define BIT5 0x20
#define BIT6 0x40
#define BIT7 0x80

#ifndef F_CPU
#define F_CPU 16000000UL /* Crystal 16.000 Mhz */
#endif

#define VITESSE_MAX 200
#define ANGLE_RX_MAX 180
#define ANGLE_COEFFICIENT 2
#define ANGLE_MAX 360
#define INT_ANGLE_TO_FLOAT(angle) (angle * 0.03490658504) // (2 * pi)/180

#define PWM_TO_DUTY_CYCLE(pwm)	((uint8_t)((pwm + 1) * 50))
/*#define PWM_TO_DUTY_CYCLE(pwm)	((uint8_t)(pwm  * 100))*/
#define NEGATIVE_PWM_TO_DUTY_CYCLE(pwm)	((uint8_t)(pwm * -100.0))

#define GAUCHE 0
#define DROITE 1

#define OUTPUT 0xFF
#define INPUT 0x00
#define FALSE 0
#define TRUE !FALSE

#define SETBIT(ADDRESS,BIT)	(ADDRESS |= (1<<BIT))
#define SETBYTE(ADDRESS,VALUE) (ADDRESS = VALUE)

#define CLEARBIT(ADDRESS,BIT) (ADDRESS &= ~(1<<BIT))
#define CLEARBYTE(ADDRESS) (ADDRESS = 0x00)

#define FLIPBIT(ADDRESS,BIT) (ADDRESS ^= (1<<BIT))
#define FLIPBYTE(ADDRESS) (~ADDRESS)

#define CHECKBIT(ADDRESS,BIT) (ADDRESS & (1<<BIT))
#define CHECKBYTE(ADDRESS) (ADDRESS)

#define WRITEBIT(RADDRESS,RBIT,WADDRESS,WBIT) (CHECKBIT(RADDRESS,RBIT) ? SETBIT(WADDRESS,WBIT) : CLEARBIT(WADDRESS,WBIT))
#define WRITEBYTE(RADDRESS,RVALUE,WADDRESS,WVALUE) (CHECKBYTE(RADDRESS,RVALUE) ? SETBYTE(WADDRESS,WVALUE) : CLEARBYTE(WADDRESS,WVALUE))

#define PORT_A (* (volatile port_bits *) &PORTA)
#define PIN_A (* (volatile port_bits *) &PINA)
#define DDR_A (* (volatile port_bits *) &DDRA)

#define PORT_B (* (volatile port_bits *) &PORTB)
#define PIN_B (* (volatile port_bits *) &PINB)
#define DDR_B (* (volatile port_bits *) &DDRB)

#define PORT_C (* (volatile port_bits *) &PORTC)
#define PIN_C (* (volatile port_bits *) &PINC)
#define DDR_C (* (volatile port_bits *) &DDRC)

#define PORT_D (* (volatile port_bits *) &PORTD)
#define PIN_D (* (volatile port_bits *) &PIND)
#define DDR_D (* (volatile port_bits *) &DDRD)

#define LED (* (volatile led_bits *) &PORTB)
#define LED_PIN PINB
#define LED_DDR DDRB
#define LED_ON  0x00
#define LED_OFF 0xFF

#define MOTOR (* (volatile motor_bits *) &PORTD)
#define MOTOR_PIN (* (volatile motor_bits *) &PIND)
#define MOTOR_DDR (* (volatile motor_bits *) &DDRD)

#endif /* DEFINE_H_ */
