/************************************************************************************
* File : TWI.h
* Author: Olivier Belisle (BELO21098902)
* Author: Felix Champagne-Lapointe (CHAF11058908)
************************************************************************************/
#ifndef TWI_H_
#define TWI_H_

/***********************************************************************************
* DEFINES
***********************************************************************************/
// Master Transmitter status#define START_STATUS 0x08
#define REP_START_STATUS 0x10
#define SLA_W_ACK_STATUS 0x18
#define SLA_W_NACK_STATUS 0x20#define ACK_W_RECEIVE_STATUS 0x28#define NACK_W_RECEIVE_STATUS 0x30#define ARBITRATION_LOST_STATUS 0x38// Master Receive status#define SLA_R_ACK_STATUS 0x40
#define SLA_R_NACK_STATUS 0x48#define ACK_R_RECEIVE_STATUS 0x50#define NACK_R_RECEIVE_STATUS 0x58#define TWSR_MASK 0xF8#define WRITE 0x00#define READ 0x01
//Prototypes
void InitTWI(void);
void Start_Write_Communication(uint8_t addr, uint8_t* data, uint8_t lenght);
void Start_Read_Communication(uint8_t addr, uint8_t* data, uint8_t lenght);

#endif /* TWI_H_ */